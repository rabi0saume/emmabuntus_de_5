https://github.com/ebruck/radiotray-ng

To Build on Debian:

Install these packages:
-----------------------------------

sudo apt install -y git
sudo apt install -y lsb-release libcurl4-openssl-dev libjsoncpp-dev libxdg-basedir-dev libnotify-dev libboost-filesystem-dev libgstreamer1.0-dev libappindicator3-dev libboost-log-dev libboost-program-options-dev libgtk-3-dev libnotify-dev lsb-release libbsd-dev libncurses5-dev libglibmm-2.4-dev libwxgtk3.0-gtk3-dev libwxgtk3.0-gtk3-0v5 cmake


Build Radiotray-NG & Debian Package
-----------------------------------

git clone https://github.com/ebruck/radiotray-ng.git
cd radiotray-ng
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make package


Convert RadioTray Bookmarks
-----------------------------------

The rt2rtng script will convert your RadioTray bookmarks.xml file. All groups within groups are moved to the root and any empty ones are removed. It's not a perfect conversion and will no doubt require some editing.

rt2rtng ~/.local/share/radiotray/bookmarks_fr.xml > bookmarks_fr.json
rt2rtng ~/.local/share/radiotray/bookmarks_all.xml > bookmarks_all.json
